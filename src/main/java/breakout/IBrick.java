package breakout;



public interface IBrick {

  int points();

  int hits();

}
